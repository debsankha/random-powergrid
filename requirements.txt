igraph==0.9.9
matplotlib==3.5.1
matplotlib-inline==0.1.3
networkx==2.6.3
numpy==1.22.2
scipy==1.8.0
