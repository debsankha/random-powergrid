.. random-powergrid documentation master file, created by
   sphinx-quickstart on Wed May  3 15:03:49 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to random-powergrid's documentation!
============================================

This work is based on the network creation algorithm published in:

A Random Growth Model for Power Grids and Other Spatially Embedded Infrastructure Networks
Paul Schultz, Jobst Heitzig, and Juergen Kurths
Eur. Phys. J. Special Topics on "Resilient power grids and extreme events" (2014)
DOI: 10.1140/epjst/e2014-02279-6

The single-node basin stability predictor has appeared here:

Detours around basin stability in power networks
Paul Schultz, Jobst Heitzig, and Juergen Kurths
New J. Phys. 16, 125001 (2014).
DOI: 10.1088/1367-2630/16/12/125001

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
